from solango.solr.function import FunctionBase


class GeoDist(FunctionBase):
    function = 'geodist(%(field)s,%(lat)s,%(lon)s) %(params)s'

    def __init__(self, field, lat, lon, *args):
        super(GeoDist, self).__init__()
        self.params['field'] = field
        self.params['lat'] = lat
        self.params['lon'] = lon
        self.params['params'] = ' '.join(args)
