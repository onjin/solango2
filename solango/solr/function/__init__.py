class FunctionBase(object):
    function = ''

    def __init__(self):
        self.params = {}

    def __unicode__(self):
        return self.function % self.params


class MinMaxBase(FunctionBase):

    def __init__(self, *args):
        super(MinMaxBase, self).__init__()

        params = []
        for idx, arg in enumerate(args):
            param = 'p%d' % (idx,)
            params.append(param)
            self.params[param] = unicode(arg)

        params = ','.join(['%(%s)s' % param for param in params])
        self.function = self.function.replace(u'++params++', params)


class Min(MinMaxBase):
    function = u'min(++params++)'


class Max(MinMaxBase):
    function = u'max(++params++)'

