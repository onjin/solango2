from solango.solr.fq import FqBase


class SpatialBase(FqBase):
    def __init__(self, pt, field, d):
        super(SpatialBase, self).__init__()
        self.params['field'] = field
        self.params['distance'] = d

        if isinstance(pt, (tuple, list)):
            self.params['lat'] = pt[0]
            self.params['lon'] = pt[1]
        elif hasattr(pt, 'lat') and hasattr(pt, 'lon'):
            self.params['lat'] = pt.lat
            self.params['lon'] = pt.lon


class GeoFilt(SpatialBase):
    command = "{!geofilt pt=%(lat)s,%(lon)s sfield=%(field)s d=%(distance)s}"


class BBox(SpatialBase):
    command = "{!bbox pt=%(lat)s,%(lon)s sfield=%(field)s d=%(distance)s}"
