from django.dispatch import Signal

solr_update = Signal(providing_args=["instance", "sender"])